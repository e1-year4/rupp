<?php

namespace App\Http\Livewire\User;

use Livewire\Component;

class CreateIncome extends Component
{
    public function render()
    {
        return view('livewire.user.create_income')->layout('layouts.base');
        // return view('livewire.user.user-dashboard-component')->layout('layouts.base');

    }
}
