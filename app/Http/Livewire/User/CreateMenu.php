<?php

namespace App\Http\Livewire\User;

use App\Models\Menu;
// use GuzzleHttp\Psr7\Request;
use Livewire\Component;
use Illuminate\Http\Request;
use File;
  

class CreateMenu extends Component
{
    public function render()
    {
        return view('livewire.user.create-menu')->layout('layouts.base');
    }

    public function store(Request $request){
        $menu = new Menu;
        $menu->fname = $request->input('fname');
        $menu->fdetail = $request->input('fdetail');
        $menu->fprice = $request->input('fprice');
        $menu->ftype = $request->input('ftype');
        $menu->updated_at = $request->input('updated_at');
        $menu->created_at = $request->input('created_at');

        if($request->hasfile('fimage')){
            $file = $request->file('fimage');
            $extenstion = $file->getClientOriginalExtension();
            $filename = time().'.'.$extenstion;
            $file->move('uploads/menu/',$filename);
            $menu->fimage = $filename;
        }
        $menu->save();
        return redirect()->back()->with('status','Menu added Successfully');
    }
 
     public function edit($id) {
        $menu = Menu::find($id);
        return view('livewire.user.edit-menu', ['menus' => $menu]);
    }

    public function update(Request $request, $id) {
        $request->validate([
            'fname' => 'required',
            'fdetail' => 'required',
            'fprice' => 'required',
            'ftype' => 'required',
            'fimage' => 'required',
        ]);
        
        $menu =Menu::find($id);
        $menu->fname = $request->input('fname');
        $menu->fdetail = $request->input('fdetail');
        $menu->fprice = $request->input('fprice');
        $menu->ftype = $request->input('ftype');
 
        if($request->hasfile('fimage')){
            $destination = 'uploads/menu/'.$menu->fimage;
            if(File::exists($destination))
            {
                File::delete($destination);
            }

            $file = $request->file('fimage');
            $extenstion = $file->getClientOriginalExtension();
            $filename = time().'.'.$extenstion;
            $file->move('uploads/menu/',$filename);
            $menu->fimage = $filename;
        }
        $menu->update();
           return redirect()->back()->with('status','Updated Successfully');
    }

    public function delete_menu($id){
        $menu = Menu::find($id);
        $menu->delete();
        return redirect()->back()->with('status','Delete Image Updated Successfully');
    }
 
}
?>