<?php

namespace App\Http\Livewire\User;

use App\Models\Video;
use Livewire\Component;

class VideoList extends Component
{
    public function render()
    {
        $videos = Video::all();
        return view('livewire.user.video',compact('videos'))->layout('layouts.base');
    }
}
?>