<?php

namespace App\Http\Livewire\User;

use App\Models\Member;
use Livewire\Component;

class MemberList extends Component
{
    public function render()
    {
        $member = Member::all();
        return view('livewire.user.member',compact('member'))->layout('layouts.base');
    }
    // public function update(Request $request, $id){
    //     $member = Member::fine($id);
    //     $member->name = $request->input('name');
    //     $member->position = $request->input('position');

    //     if($request->hasfile('profile')){
    //         $destination = 'uploads/member/'.$member->profile;
    //         if(File::exists($destination))
    //         {
    //             File::delete($destination);
    //         }

    //         $file = $request->file('profile');
    //         $extenstion = $file->getClientOriginalExtension();
    //         $filename = time().'.'.$extenstion;
    //         $file->move('uploads/member/',$filename);
    //         $member->profile = $filename;
    //     }
    //     $member->update();
    //     return redirect()->back()->with('status','Member Image Updated Successfully');
    // }
}
?>
