<?php

namespace App\Http\Livewire\User;

use App\Models\Menu;
use Livewire\Component;

class MenuList extends Component
{
    public function render()
    {
        $menus = Menu::all();
        return view('livewire.user.menu',compact('menus'))->layout('layouts.base');
    }
}
?>