<?php

namespace App\Http\Livewire\User;

use App\Models\Footer;
use Livewire\Component;

class FooterList extends Component
{
    public function render()
    {
        $footers = Footer::all();
        return view('livewire.user.footer',compact('footers'))->layout('layouts.base');
    }
}
?>