<?php

namespace App\Http\Livewire\User;

use App\Models\Footer;
// use GuzzleHttp\Psr7\Request;
use Livewire\Component;
use Illuminate\Http\Request;
use File;

class CreateFooter extends Component
{
    public function render()
    {
        return view('livewire.user.create-footer')->layout('layouts.base');
    }

    public function store(Request $request){
        $footer = new Footer;
        $footer->vname = $request->input('footerdetail');
        $footer->titletime = $request->input('titletime');
        $footer->day1 = $request->input('day1');
        $footer->day2 = $request->input('day2');
        $footer->day3 = $request->input('day3');
        $footer->footersocial = $request->input('footersocial');
        $footer->soc1 = $request->input('soc1');
        $footer->soc2 = $request->input('soc2');
        $footer->copyright = $request->input('copyright');

        if($request->hasfile('logo')){
            $file = $request->file('logo');
            $extenstion = $file->getClientOriginalExtension();
            $filename = time().'.'.$extenstion;
            $file->move('uploads/footer/',$filename);
            $footer->logo = $filename;
        }
        $footer->save();
        return redirect()->back()->with('status','Video added Successfully');
    }
 
     public function edit($id) {
        $footer = Footer::find($id);
        return view('livewire.user.edit-footer', ['footers' => $footer]);
    }

    public function update(Request $request, $id) {
        $request->validate([
            'footerdetail' => 'required',
            'logo' => 'required',
            'titletime' => 'required',
            'day1' => 'required',
            'day2' => 'required',
            'day3' => 'required',
            'time1' => 'required',
            'time2' => 'required',
            'time3' => 'required',
            'footersocial' => 'required',
            'soc1' => 'required',
            'soc2' => 'required',
            'copyright' => 'required',
        ]);
        
        $footer =Footer::find($id);
        // $footer->footerdetail = $request->input('footerdetail');
        $footer->footerdetail = $request->input('footerdetail');
        $footer->titletime = $request->input('titletime');
        $footer->day1 = $request->input('day1');
        $footer->day2 = $request->input('day2');
        $footer->day3 = $request->input('day3');
        $footer->time1 = $request->input('time1');
        $footer->time2 = $request->input('time2');
        $footer->time3 = $request->input('time3');
        $footer->footersocial = $request->input('footersocial');
        $footer->soc1 = $request->input('soc1');
        $footer->soc2 = $request->input('soc2');
        $footer->copyright = $request->input('copyright');

        if($request->hasfile('logo')){
            $destination = 'uploads/footer/'.$footer->logo;
            if(File::exists($destination))
            {
                File::delete($destination);
            }

            $file = $request->file('logo');
            $extenstion = $file->getClientOriginalExtension();
            $filename = time().'.'.$extenstion;
            $file->move('uploads/footer/',$filename);
            $footer->logo = $filename;
        }
        
        $footer->update();
           return redirect()->back()->with('status','Updated footer Successfully');
    }

    public function delete_footer($id){
        $footer = Footer::find($id);
        $footer->delete();
        return redirect()->back()->with('status','Delete Video Successfully');
    }
 
}
?>