<?php

namespace App\Http\Livewire\User;

use App\Models\Video;
// use GuzzleHttp\Psr7\Request;
use Livewire\Component;
use Illuminate\Http\Request;
  

class CreateVideo extends Component
{
    public function render()
    {
        return view('livewire.user.create-video')->layout('layouts.base');
    }

    public function store(Request $request){
        $video = new Video;
        $video->vname = $request->input('vname');
        $video->vdetail = $request->input('vdetail');
        $video->utubelink = $request->input('utubelink');
        $video->created_at = $request->input('created_at');
        $video->updated_at = $request->input('updated_at');

         
        $video->save();
        return redirect()->back()->with('status','Video added Successfully');
    }
 
     public function edit($id) {
        $video = Video::find($id);
        return view('livewire.user.edit-video', ['videos' => $video]);
    }

    public function update(Request $request, $id) {
        $request->validate([
            'vname' => 'required',
            'vdetail' => 'required',
            'utubelink' => 'required',
        ]);
        
        $video =Video::find($id);
        $video->vname = $request->input('vname');
        $video->vdetail = $request->input('vdetail');
        $video->utubelink = $request->input('utubelink');
  
        
        $video->update();
           return redirect()->back()->with('status','Updated video Successfully');
    }

    public function delete_video($id){
        $video = Video::find($id);
        $video->delete();
        return redirect()->back()->with('status','Delete Video Successfully');
    }
 
}
?>