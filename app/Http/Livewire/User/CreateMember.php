<?php

namespace App\Http\Livewire\User;

use App\Models\Member;
// use GuzzleHttp\Psr7\Request;
use Livewire\Component;
use Illuminate\Http\Request;
use File;

class CreateMember extends Component
{
    public function render()
    {
        return view('livewire.user.create-member')->layout('layouts.base');
    }

    public function store(Request $request){
    $request->validate([
        'name' => 'required',
        'position' => 'required',
        'profile' => 'required',
        
    ]);
    // Member::create($request->all());
        $member = new Member;
        $member->name = $request->input('name');
        $member->position = $request->input('position');

        if($request->hasfile('profile')){
            $file = $request->file('profile');
            $extenstion = $file->getClientOriginalExtension();
            $filename = time().'.'.$extenstion;
            $file->move('uploads/member/',$filename);
            $member->profile = $filename;
        }
        $member->save();
        return redirect()->back()->with('status','Member added Successfully');
    }
 
    public function edit($id) {
        $member = Member::find($id);
        // dd($member);
        return view('livewire.user.edit-member', ['member' => $member]);
      
    }
    
    public function update(Request $request, $id) {
        $request->validate([
            'name' => 'required',
            'position' => 'required',
            'profile' => 'required',
        ]);
        
        $member =Member::find($id);
        $member->name = $request->input('name');
        $member->position = $request->input('position');

        if($request->hasfile('profile')){
            $destination = 'uploads/member/'.$member->profile;
            if(File::exists($destination))
            {
                File::delete($destination);
            }

            $file = $request->file('profile');
            $extenstion = $file->getClientOriginalExtension();
            $filename = time().'.'.$extenstion;
            $file->move('uploads/member/',$filename);
            $member->profile = $filename;
        }
        $member->update();
           return redirect()->back()->with('status','Updated Successfully');
    }


   

    public function delete_member($id){
        $member = Member::find($id);
        $member->delete();
        return redirect()->back()->with('status','Delete Image Updated Successfully');
    }

    // public function updatemember(Member $member){
    //     // dd($member);
    //     return view('livewire.user.member',compact('member'))->layout('layouts.base');
    // }
}
?>