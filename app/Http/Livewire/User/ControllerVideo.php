<?php

namespace App\Http\Livewire\User;
use App\Models\Video;
use Livewire\Component;
use Illuminate\Http\Request;


class ControllerVideo extends Component
{
    public function render()
    {
        $member = Video::all();
        return view('livewire.user.create-video',compact('member'))->layout('layouts.base');
    }
   

    public function store(Request $request){
        $request->validate([
            'utubelink' => 'required',
            'vname' => 'required',
            'vdetail' => 'required',
        ]);
        Video::create($request->all());
        
            $video = new Video;
            $video->utubelink = $request->input('utubelink');
            $video->vname = $request->input('vname');
            $video->vdetail = $request->input('vdetail');
    
            
            $video->save();
            return redirect()->back()->with('status','video added Successfully');
        }
     
        public function edit($id) {
            $video = Video::find($id);
            // dd($member);
            return view('livewire.user.edit-video', ['video' => $video]);
            // return view('livewire.user.edit-member', compact('member'));      working
            // return view('livewire.user.updatemember',compact('member'));
        }
        
        public function update(Request $request, $id) {
            $request->validate([
                'utubelink' => 'required',
                'vname' => 'required',
                'vdetail' => 'required',
            ]);
           
            $video = Video::find($id);
            $video->utubelink = $request->input('utubelink');
            $video->vname = $request->input('vname');
            $video->vdetail = $request->input('vdetail');
    
            
            $video->update();
               return redirect()->back()->with('status','Updated Successfully');
        }
    
    
       
    
        public function delete_video($id){
            $video = Video::find($id);
            $video->delete();
            return redirect()->back()->with('status','Delete video Successfully');
        }
}
?>