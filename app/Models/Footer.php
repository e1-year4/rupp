<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Footer extends Model
{
    use HasFactory;
    protected $table = 'footers';
    protected $fileable = [
        'logo',
        'footerdetail',
        'titletime',
        'day1',
        'day2',
        'day3',
        'time1',
        'time2',
        'time3',
        'footersocial',
        'soc1',
        'soc2',
        'copyright',
    ];
}
?>