@extends('layouts.header-pages')

@section('title', 'ម្ហូបខ្មែរ')

@section('content')
   	<!-- Section Main -->
		<section id="breadcrumb" class="main_cover_page">
			<div>
				<h1 class="section-title white-font text-center">ទំនាក់ទំនង</h1>
				<ul>
					<li><a href="index.html">ទំព័រដើរ</a></li>
					<li>អំពីពួកយើង</li>
				</ul>
			</div>
		</section>
		<!-- End Section Main -->

	 
		<section id="contact-detail" class=" padd-100">
			<div class="container">
				<div class="row">
					
					<div class="col-md-3 contact-info">
						<h2 class="text-center">
							ទំនាក់ទំនងព័ត៏មានលំអិត
						</h2>
						<div class="phone text-center card-info">
							<span><i class="fas fa-phone-alt"></i></span>
							<p>លេខទូរស័ព្ទ</p>
							<p>096 336 0945</p>
						</div>
						<div class="phone text-center card-info">
							<span><i class="far fa-mail-bulk"></i></span>
							<p>អុីមែល</p>
							<p>team.e1@rupp.edu.kh</p>
						</div>
						<div class="phone text-center card-info">
							<span><i class="fas fa-map-marker-alt"></i></span>
							<p>ទីតាំង</p>
							<p>RUPP Auditorium, មហាវិថី សហពន្ធ័រុស្ស៊ី (១១០), រាជធានី​ភ្នំពេញ</p>
						</div>

					</div>

					<div class="col-md-9">
						<h2 class="text-center">បញ្ចេញមតិកែលំអរ</h2>
						<div class="form" id="form-contact">
							<form action="php/mail.php" method="post">
								<div class="column">
									<div class="col-md-6 col-sm-6">
										<input type="text" name="name" placeholder="ឈ្មោះ​" class="required-field form_sms">
									</div>
									<div class="col-md-6 col-sm-6">
										<input type="text" name="email" placeholder="អុីមែល" class="required-field form_sms">
									</div>
								</div>
								<div class="column">
									<div class="col-md-6 col-sm-6">
										<input type="text" name="sujet" placeholder="ប្រធានបទ" class="required-field form_sms">
									</div>
									<div class="col-md-6 col-sm-6">
										<input type="text" name="phone" placeholder="លេខទូរស័ព្ទ"
											class="required-field form_sms">
									</div>
								</div>
								<div class="column">
									<textarea name="message" placeholder="ខ្លឹមសារ" class="required-field form_sms feedback-sms"></textarea>
								</div>
								<p class="text-center padd-top-30">
									<button type="submit" class="btn-food">ផ្ញើសារ</button>
								</p>
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-sm-12">
					<h2 class="text-center">
						ផែនទី google
					</h2>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3908.7705869443344!2d104.88850131429278!3d11.568297147241926!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3109519fe4077d69%3A0x20138e822e434660!2sRUPP%20(Royal%20University%20of%20Phnom%20Penh)!5e0!3m2!1sen!2skh!4v1667408216795!5m2!1sen!2skh" width="100%" height="350" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
				</div>
			</div>

		</section>

@stop
