@extends('layouts.header-pages')

@section('title', 'ម្ហូបខ្មែរ')

<!-- Slide -->
<section id="main-slider" class="parallax-window">
    <div class="section-slogan">
        <h2>គេហទំព័រមុខម្ហូបខ្មែរ</h2>
        <h3>សូមស្វាគមន៏មកកាន់សេវាកម្ម និង​ ការចែករំលែកចំណេះដឹងម្ហូបខ្មែរ</h3>
    </div>
</section>
<!-- End Slide -->

@section('content')
    <section id="special-offers" class="padd-100">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="offer-left main_food_home">
                        <span class="section-suptitle">រីករាយជាមួយថ្ងៃដល្អរបស់អ្នក</span>
                        <h2 class="section-title">បង្អែមពិសេសៗសម្រាប់អ្នក</h2>
                        <div class="offer-item">
                            <img src="assets/img/demo/dessert/1.png" alt="" class="img-responsive">
                            <div>
                                <h3>នំអាកោត្នោត</h3>
                                <p>
                                    នំអាកោត្នោត ជាស្នាដៃផលិតរបស់ខ្មែរនិងមានការគាំទ្រពីសំណាក់អតិថិជន
                                    មកពីគ្រប់ទិសទីដោយសារតែការផលិតមានអនាម័យ និងមាន រសជាតិឆ្ងាញ់ពិសា។
                                </p>
                            </div>
                            <span class="offer-price">10,000៛</span>
                        </div>
                        <div class="offer-item">
                            <img src="assets/img/demo/dessert/4.png" alt="" class="img-responsive">
                            <div>
                                <h3>សង់ខ្យាល្ពៅ</h3>
                                <p>
                                    សង់ខ្យាល្ពៅជាបង្អែមខ្មែរមួយដែលមានរសជាតិឈ្ងុយឆ្ងាញ់និងមនុស្សគ្រប់គ្នាសុទ្ធតែចូលចិត្តហើយច្រើនទទួលទានជាមួយបាយដំណើប។​
                                </p>
                            </div>
                            <span class="offer-price">15,000៛</span>
                        </div>
                        <div class="offer-item">
                            <img src="assets/img/demo/dessert/5.png" alt="" class="img-responsive">
                            <div>
                                <h3>បង្អែមគ្រាប់ត្នោត</h3>
                                <p>
                                    បង្អែមគ្រាប់ត្នោត បានញ៉ាំមួយចានច្បាស់ជាចង់ថែមមួយចានទៀត
                                </p>
                            </div>
                            <span class="offer-price">5,000៛</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 hidden-sm hidden-xs">
                    <div class="offer-right">
                        <img src="assets/img/demo/foods/4food.jpg" alt="" class="img-responsive">
                        <a href="menu.html">
                            រស់ជាតិថ្មី
                            <span>មិនអាចបំភ្លេច</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Special Offers -->

    <!-- Section Restaurant Menu -->
    <section id="restaurant-menu" class="padd-100">
        <span id="order_food" class="section-suptitle text-center"></span>
        <h2 class="section-title sep-type-2 text-center">
            បញ្ជីមុខម្ហូប
        </h2>

        <div class="container">
            <div class="row">
                <ul class="restaurant-filter">
                    <li><a href="" data-filter="lunch">អាហារថ្ងៃត្រង់</a></li>
                    <li><a href="" data-filter="dinner">អាហារពេលល្ងាច</a></li>
                    <li><a href="" data-filter="drinks">ភេសជ្ជៈ</a></li>
                    <li><a href="" data-filter="dessert">បង្អែមខ្មែរនិងបរទេស</a></li>
                    <li><a href="" class="current" data-filter="">បង្ហាញអាហារទាំងអស់</a></li>
                </ul>
                <div class="restaurant-list">
                    <div class="grid-sizer col-sm-6 col-md-4"></div>
           



                    <!-- dinner -->
                    <?php
                    foreach ($menus as $menu)
                    {?>
                    <div class="col-sm-6 col-md-4 grid-item" data-filter="{{$menu->ftype,'dinner'}}">
                        <div>
                            <a href="menu.html" target="_blank"> <img src="{{ asset('uploads/menu/' . $menu->fimage) }}"
                                alt="profile img"></a>
                                    <span>តម្លៃ {{ $menu->fprice }}</span>
                                    <h3>{{ $menu->fname }}</h3>
                        </div>
                    </div>
                    <?php } ?>

                  

                      
                    {{-- <div class="col-sm-6 col-md-4 grid-item" data-filter="drinks">
                        <div>
                            <a href="menu.html" target="_blank"><img src="assets/img/demo/juices/j1.jpg" alt=""></a>
                            <span>តម្លៃ​ ៥០០០៛</span>
                            <h3>ទឹកផ្លែប៉ោម</h3>
                        </div>
                    </div> --}}

                    {{-- <div class="col-sm-6 col-md-4 grid-item" data-filter="drinks">
                        <div>
                            <a href="menu.html" target="_blank"><img src="assets/img/demo/juices/j2.jpg" alt=""></a>
                            <span>តម្លៃ​ ៥០០០៛</span>
                            <h3>ទឹកផ្លែត្រសក់ស្រូវ</h3>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 grid-item" data-filter="drinks">
                        <div>
                            <a href="menu.html" target="_blank"><img src="assets/img/demo/juices/j3.jpg" alt=""></a>
                            <span>តម្លៃ​ ៥០០០៛</span>
                            <h3>ទឹកផ្លែទំពាំងបាយជូរ</h3>
                        </div>
                    </div> --}}




                </div>
            </div>
        </div>
    </section>

    <!-- Section Event -->
    <section id="events" class="padd-100">

        <h2 class="section-title sep-type-2 text-center">ប្លុកយើង និង​​ ការចែករំលែក</h2>
        <div class="container">
            <div class="row">
                <div class="col-sm-12  no-padd slide_to_blog">

                    <ul class="event-carousel">

                        <li class="event-item">
                            <img src="assets/img/demo/dessert/5.png" alt="" class="img-responsive">
                            <div>
                                <h3>វិធីធ្វើបង្អែមគ្រាប់ត្នោត ឆ្ងាញ់ពិសា!</h3>
                                <span>ថ្ងែទី 10 តុលា 2022</span>
                                <p>
                                    ង្អែមខ្មែរយើងមានច្រើនមុខណាស់ ហើយបើនិយាយពីបង្អែមគ្រាប់ត្នោត នេះវិញគ្រាន់តែឃើញ
                                    គឺទំនងចង់ញ៉ាំតែម្ដង។
                                </p>
                                <a href="blog2.html">អានបន្ត</a>
                            </div>
                        </li>
                        <li class="event-item">
                            <img src="assets/img/demo/dessert/4.png" alt="" class="img-responsive">
                            <div>
                                <h3>វិធីធ្វើសង់ខ្យាល្ពៅ</h3>
                                <span>ថ្ងែទី 10 តុលា 2022</span>
                                <p>
                                    សង់ខ្យាល្ពៅគឺជាបង្អែមខ្មែរមួយដែលប្រជាជនខ្មែរតែងនិយមធ្វើដោយសារវាមិនចំណាយពេលវេលាឬវិធីច្រើនក្នុងការធ្វើ។
                                </p>
                                <a href="blog3.html">អានបន្ត</a>
                            </div>
                        </li>
                        <li class="event-item">
                            <img src="assets/img/demo/dessert/6.png" alt="" class="img-responsive">
                            <div>
                                <h3>របៀបធ្វើបង្អែមចេកខ្ទិះអោយឆ្ងាញ់</h3>
                                <span>ថ្ងែទី 10 តុលា 2022</span>
                                <p>
                                    ចេក គឺ​ជា​ផ្លែឈើ​ម្យ៉ាង​ដែល​សម្បូរ​ទៅ​ដោយវីតា​មីន
                                    ដែល​ផ្តល់​អត្ថប្រយោជន៍​យ៉ាង​ច្រើន​សម្រាប់​​សុខ​ភាព​ ... </p>
                                <a href="blog4.html">អានបន្ត</a>
                            </div>
                        </li>
                        <!-- more item food -->
                        <li class="event-item">
                            <img src="assets/img/demo/foods/1.jpg" alt="" class="img-responsive">
                            <div>
                                <h3>ទឹកគ្រឿងអន្លក់បន្លែស្រស់</h3>
                                <span>ថ្ងែទី 10 តុលា 2022</span>
                                <p>
                                    ទឹកគ្រឿង​អន្លក់បន្លែស្រស់ ប្រជាជនខ្មែរយើងចូលចិត្តធ្វើពិសារពិព្រោះ
                                    មាន​ភាពងាយស្រួល​ក្នុង​ការចម្អិន ប៉ុន្តែ​មាន​រសជាតិ​ឆ្ងាញ់ ជាមួយនឹង
                                    ​បន្លែស្រស់ៗ​ច្រើន​ប្រភេទ​ធ្វើជា​អន្លក់​បន្ថែម​ភាព​ទាក់ទាញ។
                                </p>
                                <a href="blog5.html">អានបន្ត</a>
                            </div>
                        </li>
                        <li class="event-item">
                            <img src="assets/img/demo/foods/fresh_rice.jpg" alt="" class="img-responsive">
                            <div>
                                <h3>បាយសាច់ជ្រូក</h3>
                                <span>ថ្ងែទី 10 តុលា 2022</span>
                                <p>
                                    បាយស្រូប ឬ បាយសាច់ជ្រូក ជាអាហារពេលព្រឹកដ៏ពេញនិយម
                                    របស់បងប្អូនប្រជាពលរដ្ឋនៅកម្ពុជា ដែលទទួលបានការទទួលទានពីគ្រប់មជ្ឈដ្ឋាន
                                    មានទាំងមនុស្សចាស់ អ្នកវ័យកណ្ដាល និង កុមារា កុមារី​ ក៏ដូចជាសិស្សសាលា ជាដើម
                                    ដោយតែងតែហូបក្រោយពេលរៀបចំខ្លួននៅពេលព្រឹក មុននឹងរៀបទៅបំពេញកិច្ចការងារផ្សេងៗ
                                    ឬ ទៅសាលារៀន ។
                                </p>
                                <a href="blog6.html">អានបន្ត</a>
                            </div>
                        </li>
                        <li class="event-item">
                            <img src="assets/img//demo/foods/19.jpg" alt="" class="img-responsive">
                            <div>
                                <h3>វិធីធ្វើសម្លរម្ជូរគ្រឿងសាច់គោ</h3>
                                <span>ថ្ងែទី 10 តុលា 2022</span>
                                <p>
                                    សម្លរម្ជូរគ្រឿងសាច់គោជាមុខម្ហូបមួយប្រភេទដែលមានរស់ជាតិឆ្ងាញ់
                                </p>
                                <a href="blog6.html">អានបន្ត</a>
                            </div>
                        </li>

                        <li class="event-item">
                            <img src="assets/img/20.jpg" alt="" class="img-responsive">

                            <h3>ប្រហុកខ្ទិះខ្មែរ</h3>
                            <span>ថ្ងែទី 10 តុលា 2022</span>
                            <p>
                                ង្អែមខ្មែរយើងមានច្រើនមុខណាស់ ហើយបើនិយាយពីបង្អែមគ្រាប់ត្នោត នេះវិញគ្រាន់តែឃើញ
                                គឺទំនងចង់ញ៉ាំតែម្ដង។
                            </p>
                            <a href="blog8.html">អានបន្ត</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- Section From The Menu -->
    <section id="from-menu" class="padd-100">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="f-menu-list">
                        <div class="f-menu-item">
                            <div class="item-left">
                                <img src="assets/img/demo/dishes/01.jpg" alt="">

                                <!-- <iframe class="youtube-video" width="560" height="315" src="https://www.youtube.com/embed/LrJbrydDf2s?enablejsapi=1&version=3&playerapiid=ytplayer" modestbranding="0"  controls="0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->

                            </div>
                            <div class="item-right">
                                <span class="section-suptitle">ចង់ញុំអ្វីដែរជាស្នាដៃខ្មែរ</span>
                                <h3 class="section-title">
                                    សម្លការីខ្មែរ
                                </h3>
                                <h4>រូបមន្ត និង គ្រឿងផ្សំ</h4>
                                <p>
                                    សម្លរការីសាច់មាន់ ជាមុខម្ហូបមួយប្រភេទ
                                    ដែលប្រជាជនខ្មែរតែងតែធ្វើរាល់ពេលមានកម្មវិធីជួបជុំម្ដង។ ភាគច្រើនសម្លរនេះ
                                    រាងពិបាកធ្វើបន្តិច ព្រោះតម្រូវមានគ្រឿងផ្សំច្រើន។ តាមការនិយមចូលចិត្ត
                                    សម្លរការីសាច់មាន់អាចដាក់បន្ថែមសាច់ជ្រូកខ្លះក៏បានដែរ។ ដូច្នេះ
                                    ដើម្បីដឹងពីរបៀបនៃការធ្វើសម្លរការីសាច់មាន់ សូមចុចមើលវីឌីអូការណែនាំ</p>
                                <a href="\video">ទៅកាន់វីឌីអូ</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
    </section>
@stop
