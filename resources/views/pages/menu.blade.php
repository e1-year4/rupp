@extends('layouts.header-pages')

@section('title', 'ម្ហូបខ្មែរ')

@section('content')
    <style>
        .offer-item {
            align-items: center;
            display: flex;
        }
    </style>
    <!-- Site Wrapper -->
    <div id="site-wrapper">

        <!-- Section Main -->
        <section id="breadcrumb" class="main_cover_page">
            <div>
                <h1 class="section-title white-font text-center">បញ្ជីមុខម្ហូប</h1>
                <ul>
                    <li><a href="index.html">ទំព័រដើម</a></li>
                    <li>បញ្ជីមុខម្ហូប</li>
                </ul>
            </div>
        </section>
        <!-- End Section Main -->

        <!-- Section មុខម្ហូប -->
        <section class="main_bookmenu padd-100">
            <div class="book_menu">
                <h2 class=" section-title sep-type-2 text-center">ទំព័រមុខម្ហូប ភេសជ្ជៈ និង បង្អែម</h2>

                <div class="menu_all">
                    <div class="container">
                        <div class="row">
                            <div class="list_food">
                                <div class="col-md-6 col-sm-12">
                                    <ol>
                                        <li><a href="blog2.html">បង្អែមគ្រាប់ត្នោត</a></li>
                                        <li><a href="blog3.html">បង្អែមសង់ខ្យាល្ពៅ</a></li>
                                        <li><a href="blog4.html">បង្អែមចេកខ្ទិះ</a></li>
                                        <li><a href="blog8.html">ប្រហុកខ្ទិះខ្មែរ</a></li>

                                    </ol>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <ol>
                                        <li><a href="blog5.html">ទឹកគ្រឿងអន្លក់បន្លែស្រស់</a></li>
                                        <li><a href="blog6.html">បាយស្រូបពេលព្រឹក</a></li>
                                        <li><a href="blog7.html">សម្លរម្ជូរគ្រឿងសាច់គោ</a></li>

                                    </ol>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>

        </section>

        <section id="lunch-menu">
            <div class="container">
                <div class="row">
                    <div class="menu-carousel vertical-carousel">
                        <div class="menu-item">
                            @foreach ($menus as $val)
                                <div class="col-md-6">
                                    <div class="offer-item">
                                        <img src="{{ asset('uploads/menu/' . $val->fimage) }}" alt="{{ $val->fimage }}"
                                            class="img-responsive">
                                        <div class="listfo">
                                            <h3>{{ $val->fname }}</h3>
                                            <p>{{ $val->fdetail }}</p>
                                        </div>
										<span class="offer-price">{{ $val->fprice }}&nbsp;រៀល</span>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                    </div>
                </div>

            </div>
        </section>
        <!-- End Section Dessert -->


        <script type="text/javascript">
            function count() {
                var chkboxes = document.getElementsByName("itemn")
                var sum = 0
                for (var i = 0; i < chkboxes.length; i++) {
                    if (chkboxes[i].checked == true) {
                        sum += parseInt(chkboxes[i].value)
                    }
                }
                document.getElementById('pay').innerHTML = sum
            }
        </script>
        </head>

        <body>
            <!-- Section Drink -->
            <section id="drink" class="padd-100">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <span class="section-suptitle"></span>
                            <h4 class="section-title">
                                ភេសជ្ជៈ
                            </h4>
                            <div class="drink-item">
                                <span><label><input type="checkbox" value="5000" id="p1"
                                            onclick="UpdateCost(this);">កូកាកូលា ដបនឹងកំពុង</label></span>
                                <p>៥,០០០៛</p>
                            </div>
                            <div class="drink-item">
                                <span><label><input type="checkbox" value="6000" id="p2"
                                            onclick="UpdateCost(this);">អីឆីតាន់</span></label>
                                <p>៦,០០០៛</p>
                            </div>
                            <div class="drink-item">
                                <span><label><input type="checkbox" value="4000" id="p3"
                                            onclick="UpdateCost(this);">ទឹកក្រូច</label></span>
                                <p>៤,០០០៛</p>
                            </div>
                            <div class="drink-item">
                                <span><label><input type="checkbox" value="6500" id="p3"
                                            onclick="UpdateCost(this);">តែក្រូចឆ្មា</label></span>
                                <p>៦,៥០០៛</p>
                            </div>
                            <div class="drink-item">
                                <span><label><input type="checkbox" value="6000" id="p3"
                                            onclick="UpdateCost(this);">បោសស្រង</label></span>
                                <p>៦,០០០៛</p>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <span class="section-suptitle"></span>
                            <h4 class="section-title">
                                កាហ្វេ
                            </h4>
                            <div class="drink-item">
                                <span><label><input type="checkbox" value="6000" id="p3"
                                            onclick="UpdateCost(this);">កាហ្វេទឹកដោះគោ</label></span>
                                <p>៤,៩០០៛</p>
                            </div>
                            <div class="drink-item">
                                <span><label><input type="checkbox" value="6000" id="p3"
                                            onclick="UpdateCost(this);">កាហ្វេទឹកកក</label></span>
                                <p>៦,០០០៛</p>
                            </div>
                            <div class="drink-item">
                                <span><label><input type="checkbox" value="5400" id="p3"
                                            onclick="UpdateCost(this);">Green
                                        tea lava</label></span>
                                <p>៥,៤០០៛</p>
                            </div>
                            <div class="drink-item">
                                <span><label>
                                        <input type="checkbox" value="5400" id="p3"
                                            onclick="UpdateCost(this);">Green
                                        tea
                                    </label>
                                </span>
                                <p>៥,៤០០៛</p>
                            </div>
                            <div class="drink-item">
                                <span>
                                    <input type="checkbox" name="chocolate" id="chocolate" onclick="UpdateCost(this);"
                                        value="4500">
                                    <label for="chocolate">chocolate</label>
                                </span>
                                <p>៥,៤០០៛</p>
                            </div>
                            <div class="list_food">
                                <ul>
                                    <li>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="container">
                        <div class="col-md-6">
                            <div class="ord">
                                <ul class="list_items">
                                    <li>

                                    </li>
                                </ul>
                            </div>
                            <h3 class="text-left">តម្លៃម្ហូបនិងភេសជ្ជៈ</h3>
                            <div class="col-md-12">
                                <div class="form-inline">
                                    <div class="form-inline">
                                        <label>Total Cost</label>
                                        <input type="text" id="total" disabled="disabled" />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <!-- End Section Drink -->

            <script type="text/javascript">
                var total = 0;

                function UpdateCost(elem) {
                    if (elem.checked == true) {
                        total += Number(elem.value);
                    } else {
                        total -= Number(elem.value);
                    }
                    document.getElementById('total').value = total.toFixed(0);
                }


                //order list name
                // $('.cb').click(function () {
                // 	$('ul.list_items').html("");
                // 	$(".cb").each(function () {
                // 		if ($(this).is(":checked")) {
                // 			$('ul.list_items').append('<li>' + $(this).val() + '</li>')
                // 		}
                // 	});
                // });

                var str_name = "";
                $('.cb').click(function() {
                    $('ul.list_items').html("");
                    $(".cb").each(function() {
                        if ($(this).is(":checked")) {
                            if ($('#d1').is(":checked")) {
                                str_name = "Chocolate2";
                            }
                            if ($('#d2').is(":checked")) {
                                str_name = "tes";
                            }
                            if ($('#d3').is(":checked")) {
                                str_name = "Chocolatasbdfjjdagbe2";
                                document.getElementById("d1").disabled = true;
                            }
                            $('ul.list_items').append('<li>' + str_name + " " + $(this).val() + '</li>')

                        }
                    });
                });

                // ===================================



                // $('.cb').click(function () {
                // 	$('ul.list_items').html("");
                // 	$(".cb").each(function () {
                // 		if ($(this).is(":checked")) {
                // 			$('ul.list_items').append('<li>' + $(this).dataset.parent + '</li>')
                // 		}
                // 	});
                // });


                // list name of foods ============================================

                // function UpdateCost(elem) {
                // 	var cb = document.getElementById("d1");
                // 	var text = document.getElementById("msg_food");
                // 	if(cb.checked==true){
                // 		text.style.display="block";
                // 	} else {
                // 		text.style.display="none";
                // 	}
                // }
            </script>
        @stop
