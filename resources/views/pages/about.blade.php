@extends('layouts.header-pages')

@section('title', 'ម្ហូបខ្មែរ')

@section('content')
<style>
   .flex-wrap {
    display: flex;
    flex-wrap: wrap;
}
</style>
    <!-- Site Wrapper -->
    <div id="site-wrapper">



        <section id="breadcrumb" class="main_cover_page">
            <div>
                <h1 class="section-title white-font text-center">អំពីពួកយើង</h1>
                <ul>
                    <li><a href="index.html">ទំព័រដើរ</a></li>
                    <li>អំពីពួកយើង</li>
                </ul>
            </div>
        </section>


        <!-- End Section Main -->

        <!-- Section Story -->
        <section id="our-story">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="story-description">
                            <h2 class="section-title">គោលបំណងរបស់របស់ពួកយើង</h2>
                            <p>
                                ពួកយើងមកពី <b>សកលវិទ្យាល័យភូមិន្ទភ្នំពេញ​(ជំនាញព័ត៌មានវិទ្យា)</b>
                                បានបង្កើតគេហទំព័រមួយនេះក្នុងគោលបំណងចង់ចែករំលែកអំពីរបៀបនៃការធ្វើ​ ម្ហូប បង្អែម​ ទឹកជ្រលក់
                                ភេសជ្ជៈជាច្រើនប្រភេទបែបខ្មែរដែលអ្នកទាំងអស់គ្នាមិនទាន់ដឺងអំពីរបៀបធ្វើ
                                ដើម្បីជួយសម្រូលទៅដល់អ្នកទាំងអស់គ្នាក្នុងការរស់នៅប្រចាំថ្ងៃ...។
                            </p>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 techno_lang">
                                    <h4>ក្រុមយើងប្រើ Language Technology ដូចជា</h4>
                                    <div class="lang_list">
                                        <ul>
                                            <li><i class="far fa-code"></i><a href="#">HTML and CSS</a></li>
                                            <li><i class="far fa-code"></i><a href="https://getbootstrap.com/docs/3.3/css/"
                                                    target="_blank" rel="noopener noreferrer">Bootstrap v3</a></li>
                                            <li><i class="far fa-code"></i><a
                                                    href="https://www.npmjs.com/package/@bedrock-layout/masonry-grid"
                                                    target="_blank" rel="noopener noreferrer">plugin javascript</a></li>
                                            <li><i class="far fa-code"></i><a href="https://fontawesome.com/"
                                                    target="_blank" rel="noopener noreferrer">fontawesome v5 Js</a></li>
                                            <li><i class="far fa-code"></i><a href="https://gitlab.com/e1-year4/teame1.git"
                                                    target="_blank" rel="noopener noreferrer">Gitlab</a></li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <img src="assets/img/demo/story/team.jpg" alt="" class="img-responsive img-story">
                    </div>
                </div>



            </div>
        </section>
        <!-- End Section Story -->

        <!-- Section Vision -->
        <section id="vision" class="parallax-window padd-100">
            <h2 class="sr-only">Vision</h2>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="vision-item">
                            <span class="count">168</span>
                            <p>មុខម្ហូប</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="vision-item">
                            <span class="count">19</span>
                            <p>លិខិតសរសើរ</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="vision-item">
                            <span class="count">8</span>
                            <p>សមាជិក</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="vision-item last-item">
                            <span class="count">15</span>
                            <p>លិខិតទទូលស្គាល់</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Section Vision -->

        <!-- Section Teams -->
        <section id="team" class="padd-100">
            <h4 class="section-title sep-type-2 text-center">
                សមាជិកចូលរូមបង្កើតគេហទំព័រ
            </h4>
            <div class="container flex-wrap">
                 
                    @foreach ($member as $val)
                    <div class="col-md-3">
                        <div class="our-team">
                            <div class="pic">
                                <img src="{{ asset('uploads/member/' . $val->profile) }}" alt="profile img"
                                    width="" height="">
                            </div>
                            <h3 class="title">{{ $val->name }}</h3>
                            <span class="post">{{ $val->position }}</span>
                            <ul class="social">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-telegram-plane"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    @endforeach
            </div>
        </section>
    </div>
     

@stop
