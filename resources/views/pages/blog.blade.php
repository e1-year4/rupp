@extends('layouts.header-pages')

@section('title', 'ម្ហូបខ្មែរ')

@section('content')
  	<!-- Section Main -->
	  <section id="breadcrumb" class="main_cover_page">
		<div>
			<h1 class="section-title white-font text-center">ម្ហូបខ្មែរប្លុកយើង</h1>
			<ul>
				<li><a href="index.html">ទំព័រដើម</a></li>
				<li>ម្ហូបខ្មែរប្លុកយើង</li>
			</ul>
		</div>
	</section>
	<!-- Section Contact -->
	<section id="blog" class="padd-100">
		<h2 class="sr-only">Blog</h2>
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<article class="blog-item">
						<div class="entry-thumbnail">
							<a title="" href="#">
								<img src="assets/img/demo/blog/07.jpg" alt="" class="img-responsive">
							</a>
						</div>
						<div class="post-content">
							<h2 class="entry-title">
								កម្រងរាល់មុខម្ហូបនៅក្នុងភោជនីយដ្ឋានយើងខ្ញុំ
							</h2>
							<div class="entry-content">
								 
								<p class="extrait-post">
									ភោជនីយដ្ឋានម្ហូបខ្មែយើងខ្ញុំមានគម្រោងប្រមូលផ្ដុំនូវអាហារគ្រប់ប្រភេទនៅក្នុងប្រទេសកម្ពុជាដើម្បីជាកម្រងចងក្រងនូវស្នាដៃបុព្វបុរសពីបុរាណកាលបន្សល់ទុក
								<br>ឲ្យកូនចៅជំនាន់ក្រោយផងដែរ។
								</p>
								<a class="btn-read-more" href="blogfood.html">អានបន្ត</a>
							</div>
						</div>
					</article>
					<article class="blog-item">
						<div class="entry-thumbnail">
							<a title="" href="#">
								<img src="assets/img/demo/blog/09.jpg" alt="" class="img-responsive">
							</a>
						</div>
						<div class="post-content">
							<h2 class="entry-title">
								កម្រងរាល់ភេសជ្ជៈនៅក្នុងភោជនីយដ្ឋានយើងខ្ញុំ
							</h2>
							<div class="entry-content">
							
								<p class="extrait-post">
									រីឯភេសជ្ជៈខាងភោជនីយដ្ឋានយើងខ្មុំក៏បានដកស្រង់នូវភេសជ្ជៈពិសេសៗជាច្រើនមកបង្ហាញនិងផ្ដល់ជូនភ្ញៀវ​ក្នុងស្រុកនឹងបរទេសទាំងអស់ផងដែរ។
								</p>
								<a class="btn-read-more" href="blogdrink.html">អានបន្ត</a>
							</div>
						</div>
					</article>
					<article class="blog-item">
						<div class="entry-thumbnail imgkiki">
							<a title="" href="#">
								<img src="assets/img/demo/blog/10.jpg" alt="" class="img-responsive">
							</a>
						</div>
						<div class="post-content">
							<h2 class="entry-title">
								កម្រងរាល់បង្អែមនៅក្នុងភោជនីយដ្ឋានយើងខ្ញុំ
							</h2>
							<div class="entry-content">
								 
								<p class="extrait-post">
									ចំពោះខាងផ្នែកបង្អែមវិញភោជនីយដ្ឋានរបស់យើងខ្មុំក៏បានចាប់យកមកនូវបង្អែមល្បីៗជាច្រើនប្រភេទទាំងក្នុងប្រទេសកម្ពុជាផ្ទាល់ព្រមទាំងប្រភេទបង្អែមរបស់<br>ប្រទេសផ្សេងៗក្នុងអាស៊ីយើងផងដែរ។
								</p>
								<a class="btn-read-more" href="blogdessert.html">អានបន្ត</a>
							</div>
						</div>
					</article>
					 
				</div>
	
				<div class="col-md-4 hidden-sm hidden-xs">
					<div class="sidebar">
						<div class="widget widget_category">
							<h3 class="widget-title">ប្រភេទម្ហូប</h3>
							<ul>
								<li><a href="index.html#order_food">អាហាថ្ងៃត្រង់<span>(១៩)</span></a></li>
								<li><a href="index.html#order_food">អាហាពេលល្ងាច<span>(២១)</span></a></li>
								<li><a href="index.html#order_food">ភេសជ្ជៈ<span>(២៨)</span></a></li>
								<li><a href="index.html#order_food">បង្អែមខ្មែរនិងបរទេស<span>(១៤)</span></a></li>
								<li><a href="index.html#order_food">អាហាគ្រប់មុខ<span>(១៣)</span></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Section Contact -->


@stop
