@extends('layouts.header-pages')

@section('title', 'ម្ហូបខ្មែរ')

@section('content')

    <!-- Section Main -->
    <section id="breadcrumb" class="main_cover_page">
        <div>
            <h1 class="section-title white-font text-center">វីឌីអូ</h1>
            <ul>
                <li><a href="index.html">ទំព័រដើរ</a></li>
                <li>វីឌីអូរ និង រូបភាព</li>
            </ul>
        </div>
    </section>

    <!--  Section Video by Sokha -->
    <section class="padd-100 food_video">
        <h1 class="text-center section-title sep-type-2">វីឌីអូ</h1>
        <div class="container">
            <div class="row card_video">
                @foreach ($videos as $val)
                    <div class="col-md-3 absol">
                        <div class="card">
                            <div class="thumb-v">
                                
                                    <iframe width="auto" height="169px" src="https://www.youtube.com/embed/{{ $val->utubelink }}?autoplay=1&mute=1"
                                        title="បង្អែមចេកខ្ទិះ" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">{{ $val->vname }}</h5>
                                <p class="card-text">
                                    {{ $val->vdetail }}</p>
                                <a href="https://www.youtube.com/embed/{{ $val->utubelink }}" class="btn btn-primary">ចុចទសនា</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
        </div>

        </div>

        <!-- <div class="row card_video">
            
            <div class="col-md-3 absol">
             <div class="card">
              <div class="thumb-v">
               <iframe width="auto" height="169px" src="https://www.youtube.com/embed/EJnaV6yMJDQ?autoplay=1&mute=1" title="បង្អែមចេកខ្ទិះ" frameborder="0" allowfullscreen></iframe>
               <img src="assets/img/demo/dessert/6.png" class="card-img-top" alt="បង្អែមចេកខ្ទិះ">
              </div>
             <div class="card-body">
              <h5 class="card-title">វិធីធ្វើបង្អែមគ្រាប់ត្នោត</h5>
              <p class="card-text">បង្អែម​គ្រាប់​ត្នោត​ខ្ទិះ​ដូង គឺ​ជា​បង្អែម​សាមញ្ញ​មួយ​ប្រភេទ ដែល​មាន​រសជាតិ​ឈ្ងុយ​ឆ្ងាញ់ ហើយ​ងាយ​ស្រួល​ធ្វើ ដោយ​ប្រើ​ប្រាស់​តែ​គ្រឿង​ផ្សំ ដែល​លោក​អ្នក​អាច​រក​បាន​យ៉ាង​ងាយ​ស្រួល នៅ​ក្នុង​ទីផ្សារ​បច្ចុប្បន្ន។</p>
              <a href="https://www.youtube.com/watch?v=lzjQbwzoPrc" class="btn btn-primary">ចុចទសនា</a>
             </div>
             </div>
            </div>
            
           </div> -->
        </div>
    </section>



@stop
