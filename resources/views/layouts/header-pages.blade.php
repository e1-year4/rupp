<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="author" content="Egprojets">
    <meta name="description" content="" />
	<title>App Name - @yield('title')</title>

    <!-- Contribute CSS Files -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    {{-- <link rel="stylesheet" href="{{asset('css/bootsrap.min.css')}}"> --}}


    <!-- Custom CSS Files -->
    <link rel="stylesheet" href="assets/css/style.css" />
    <link rel="stylesheet" href="assets/css/mystyle.css" />
    <link rel="stylesheet" href="assets/css/responsive.css" />

    <!-- font khmer  -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Battambang:wght@300;400&family=Poppins&display=swap"
        rel="stylesheet">
</head>

<body>
    <header id="site-header">

        <div class="navbar" role="navigation">
            <div class="container">
                <div class="row">
                    @foreach(\App\Models\Footer::all() as $footer)
                    <h1 class="sr-only">Food Lover</h1>
                    <a href="/" title="FoodLover" class="logo">
                        <img src="{{ asset('uploads/footer/' . $footer->logo) }}"
                                        alt="profile img" width="170px"> 
                               
                        {{-- <img src="assets/img/LOGO_Khfood.png" alt="" width="170"> --}}
                    </a>
                    <button data-target=".navbar-collapse" data-toggle="collapse" type="button"
                        class="menu-mobile visible-xs">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <ul class="nav navbar-nav navbar-collapse collapse">
                        <li><a class="menu active" href="/">ទំព័រដើរ</a></li>
                        <li><a class="menu" href="/about">អំពីយើង</a></li>
                        <li><a class="menu" href="/menu">បញ្ជីមុខម្ហូប</a></li>
                        <li><a class="menu" href="/blog">ប្លុកយើង</a></li>
                        <li><a class="menu" href="/contact">ទំនាក់ទំនង</a></li>
                        <li><a class="menu" href="/video">សកម្មភាពផ្សេងៗ</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </header>
    <!-- End Header -->


    <div class="content">
        @yield('content')

            {{-- live chat fb --}}
            <!-- Messenger Chat Plugin Code -->
    
    </div>
   
    <!-- Footer -->
    <footer id="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="bloc-cms">
                        <a href="index" title="FoodLover" class="logo">
                            <img src="{{ asset('uploads/footer/' . $footer->logo) }}"
                            alt="profile img" width="170px"> 
                   
                            {{-- <img src="assets/img/LOGO_Khfood.png" alt="" width="170"> --}}
                        </a>
                        <p>
                            {{ $footer->footerdetail }}<
                        
                        </p>
                        <a class="toabout" href="about-us">អានបន្ត</a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="open-hours">
                        <span class="foot-title">{{ $footer->titletime }}<</span>
                        <p><span>{{ $footer->day1 }}< </span>{{ $footer->time1 }}<</p>
                        <p><span>{{ $footer->day2 }}< </span>{{ $footer->time2 }}<</p>
                        <p><span>{{ $footer->day3 }}< </span>{{ $footer->time3 }}<</p>
                    </div>
                </div>

                <div class="col-md-4 hidden-sm hidden-xs">
                    <span class="foot-title">{{ $footer->footersocial }}<</span>
                    <div class="socail">
                        <ul>

                            <li><a href=""><img
                                        src="assets/img/demo/logo_icons/512px-Telegram_logo.svg.png" alt=""></a>
                            </li>
                            <li><a href="{{ $footer->soc1 }}<"><img
                                        src="assets/img/demo/logo_icons/5847f997cef1014c0b5e48c1.png"
                                        alt="gitlab"></a></li>
                            <li><a href="{{ $footer->soc2 }}<"><img
                                        src="assets/img/demo/logo_icons/facebook-logo.png" alt="facebook-logo"></a></li>
                            <li><a
                                    href="https://www.instagram.com/explore/locations/225235076/rupp-royal-university-of-phnom-penh/"><img
                                        src="assets/img/demo/logo_icons/Instagram_icon.png" alt=""></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="footer-copyright">
            <p>
                
                <p>{{ $footer->copyright }}</p>
            @endforeach

            </p>
        
            <a href="">Top</a>
        </div>
    </footer>
     
    <!-- End Footer -->
    <!-- Contribute JS Files -->
    <script type="text/javascript" src="assets/js/egprojets.lib.js"></script>
    <script type="text/javascript" src="assets/js/egprojets.custom.js"></script>
    <script type="text/javascript" src="assets/js/fontawesome.js"></script>
</body>

</html>
