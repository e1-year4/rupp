@extends('layouts.basedash')

@section('content')

<div>
   
  <div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="card-body">
                @if (session('status'))
                <h6 class="alert alert-success">{{session('status')}}</h6>
                    
                @endif
                <h4>
                    <a href="{{route ('user.dashboard')}}" class="btn btn-warning btn-sm float-end">Dashboard <i class="nav-icon fas fa-tachometer-alt"></i></a>
                    <a href="{{route ('user.menu')}}" class="btn btn-warning btn-sm float-end">Listing foods<i class="nav-icon fas fa-tachometer-alt"></i></a>
                </h4>
                {{-- <form action="{{ url('user/updatemenus'.$menus->id) }}" method="POST" enctype="multipart/form-data"> --}}
                {{-- <form method="POST" action="{{ url('user/update/'.$menus->id) }}" enctype="multipart/form-data"> --}}
                <form method="POST" action="{{ route('user.update_menu', $menus->id) }}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    {{-- {{ csrf_field() }} --}}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="name">Food Name</label>
                             <input name="fname" type="text" class="form-control" id="fname" value="{{$menus->fname}}" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="ftype">Food type</label>
                            <select class="form-select" aria-label="Default select example" name="ftype" id="ftype" value="{{$menus->fname}}" required>
                                <option selected>Selects type menu</option>
                                <option value="lunch">Lunch</option>
                                <option value="dinner">Dinner</option>
                                <option value="drinks">Drink</option>
                                <option value="dessert">Dessert</option>
                            </select>
                        </div>
                    </div>
                      
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="position">Detail of food</label>
                            <input name="fdetail" type="text" class="form-control" id="fdetail" value="{{ $menus->fdetail}}" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="position">Price of food</label>
                            <input name="fprice" type="text" class="form-control" id="fprice" value="{{ $menus->fprice}}" required>
                        </div>
                    </div>
                    
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="profile">Picture</label>
                            <input name="fimage" type="file" class="form-control" id="fimage" required>
                        </div>
                    </div>
                 
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </section>
</div>
<script>
   $('#prefill').datepicker({

});
</script>
</div>
@endsection
