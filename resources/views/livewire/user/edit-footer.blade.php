@extends('layouts.basedash')

@section('content')
<style>
    .foot1,.foot2,.foot3 {
        border: 1px solid #d9d9d9;
    }
</style>
<div>
    <div class="content-wrapper">
      <section class="content">
          <div class="container-fluid">
              <div class="card-body">
                  @if (session('status'))
                  <h6 class="alert alert-success">{{session('status')}}</h6>
                      
                  @endif
                  <h4>
                      <a href="{{route ('user.dashboard')}}" class="btn btn-warning btn-sm float-end">Dashboard <i class="nav-icon fas fa-tachometer-alt"></i></a>
                      <a href="{{route ('user.footer')}}" class="btn btn-warning btn-sm float-end">Listing foods <i class="ion-navicon-round"></i></a>
                  </h4>
                  {{-- <form action="{{ url('user/updatefooters'.$footers->id) }}" method="POST" enctype="multipart/form-data"> --}}
                  {{-- <form method="POST" action="{{ url('user/update/'.$footers->id) }}" enctype="multipart/form-data"> --}}
                  <form method="POST" action="{{ route('user.update_footer', $footers->id) }}" enctype="multipart/form-data">
                      @csrf
                      @method('PUT')
                      {{-- {{ csrf_field() }} --}}
                      
                        
                       
                      
                    <footer id="site-footer">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-6 col-md-4 foot1">
                                    <label for="">Footer 1</label>
                                    <div class="bloc-cms">
                                       
                                            <div class="form-group col-md-6">
                                                <label for="profile">Logo</label>
                                                <input name="logo" type="file" class="form-control" id="logo" required>
                                            </div>
                                     
                                        <p>
                                            <textarea class="form-control" rows="10" name="footerdetail">{{$footers->footerdetail}}</textarea>
                                         </p>
                                        {{-- <a class="toabout" href="about-us.html">អានបន្ត</a> --}}
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4 foot2">
                                    <label for="">Footer 2</label>
                                    <div class="open-hours">
                                        <span class="foot-title"><input name="titletime" type="text" class="form-control" id="fname" value="{{$footers->titletime}}"></span>
                                        <p><span><input name="day1" type="text" class="form-control" id="fname" value="{{$footers->day1}}"></span><input name="time1" type="text" class="form-control" id="fname" value="{{$footers->time1}}"></p>
                                        <p><span><input name="day2" type="text" class="form-control" id="fname" value="{{$footers->day2}}"></span><input name="time2" type="text" class="form-control" id="fname" value="{{$footers->time2}}"></p>
                                        <p><span><input name="day3" type="text" class="form-control" id="fname" value="{{$footers->day3}}"></span><input name="time3" type="text" class="form-control" id="fname" value="{{$footers->time3}}"></p>
                                    </div>
                                </div>
                
                                <div class="col-md-4 hidden-sm hidden-xs foot3">
                                    <label for="">Footer 3</label>
                                    <span class="foot-title"><input name="footersocial" type="text" class="form-control" id="fname" value="{{$footers->footersocial}}"></span>
                                    <div class="socail">
                                    <span class="foot-title"><label for="">Link Git</label><input name="soc1" type="text" class="form-control" id="soc1" value="{{$footers->soc1}}"></span>
                                    <span class="foot-title"> <label for="">Link Facebook</label><input name="soc2" type="text" class="form-control" id="soc1" value="{{$footers->soc2}}"></span>

                                        {{-- <input name="copyright" type="text" class="form-control" id="copyright" value="{{$footers->copyright}}">
                                        <input name="copyright" type="text" class="form-control" id="copyright" value="{{$footers->copyright}}"> --}}

                                    </div>
                                    <div class="footer-copyright">
                                             <label for="">Copy right</label>
                                            <input name="copyright" type="text" class="form-control" id="copyright" value="{{$footers->copyright}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </footer>
                      {{-- <div class="form-row">
                          <div class="form-group col-md-6">
                              <label for="profile">Picture</label>
                              <input name="fimage" type="file" class="form-control" id="fimage" required>
                          </div>
                      </div> --}}
                   
                      <button type="submit" class="btn btn-primary">Save</button>
                  </form>
              </div>
          </div>
      </section>
  </div>
  <script>
     $('#prefill').datepicker({
  
  });
  </script>
  </div>
@endsection
