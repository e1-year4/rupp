@extends('layouts.basedash')

@section('content')
    <div>

        <div class="content-wrapper">
            <section class="content">
                <div class="container-fluid">
                    <div class="card-body">
                        @if (session('status'))
                            <h6 class="alert alert-success">{{ session('status') }}</h6>
                        @endif
                        <h4>
                            <a href="{{ route('user.create-video') }}" class="btn btn-primary btn-sm">Add</a>
                            <a href="{{ route('user.dashboard') }}" class="btn btn-warning btn-sm float-end"><i
                                    class="nav-icon fas fa-tachometer-alt"></i>Dashboard </a>
                            <a href="{{ route('user.video') }}" class="btn btn-warning btn-sm float-end"><i
                                    class="ion-navicon-round"></i> Listing foods</a>
                            <a href="{{ route('pages.video') }}" target="_blank" class="btn btn-primary btn-sm"><i
                                    class="fas fa-eye"></i> View Page </a>

                        </h4>

                        <form method="POST" action="{{ route('user.update_video', $videos->id) }}"
                            enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            {{-- {{ csrf_field() }} --}}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="utubelink">Link Youtube</label>
                                            <input name="utubelink" type="text" class="form-control" id="utubelink"
                                                value="{{ $videos->utubelink }}" required>
                                        </div>
                                    </div>
        
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="vname">Title</label>
                                            <input name="vname" type="text" class="form-control" id="vname"
                                                value="{{ $videos->vname }}" required>
                                        </div>
                                    </div>
        
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="vdetail">Detail video</label>
                                            <input name="vdetail" type="text" class="form-control" id="vdetail"
                                                value="{{ $videos->vdetail }}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="vdetail">Video</label>
                                    <div class="form-group col-md-6">
                                        <iframe width="auto" height="" src="https://www.youtube.com/embed/{{ $videos->utubelink }}?autoplay=1&mute=1" title="បង្អែមចេកខ្ទិះ" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                </div>

                            </div>
                           

                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                    </div>
                </div>
            </section>
        </div>
        <script>
            $('#prefill').datepicker({

            });
        </script>
    </div>
@endsection
