@extends('layouts.basedash')

@section('content')

<div>
    {{-- Be like member. --}}
      <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="card-body">
                @if (session('status'))
                <h6 class="alert alert-success">{{session('status')}}</h6>
                    
                @endif
                <h4>
                    <a href="{{route ('user.dashboard')}}" class="btn btn-warning btn-sm float-end">Dashboard <i class="nav-icon fas fa-tachometer-alt"></i></a>
                </h4>
                {{-- <form action="{{ url('user/updatemember'.$member->id) }}" method="POST" enctype="multipart/form-data"> --}}
                {{-- <form method="POST" action="{{ url('user/update/'.$member->id) }}" enctype="multipart/form-data"> --}}
                <form method="POST" action="{{ route('user.update', $member->id) }}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    {{-- {{ csrf_field() }} --}}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="name">Member Name</label>
                            {{-- <input name="name" type="text" class="form-control" id="name" value="{{ !! request('name') ? $suspect->name : old(name) !!}}" required> --}}
                            <input name="name" type="text" class="form-control" id="name" value="{{$member->name}}" required>
                        </div>
                    </div>
                      
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="position">Position</label>
                            <input name="position" type="text" class="form-control" id="position" value="{{ $member->position}}" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="profile">Profile Image</label>
                            <input name="profile" type="file" class="form-control" id="profile" required>
                        </div>
                    </div>
                 
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </section>
</div>
<script>
   $('#prefill').datepicker({

});
</script>
</div>
@endsection