<div>
    {{-- Be like water. --}}
      <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="card-body">
                @if (session('status'))
                <h6 class="alert alert-success">{{session('status')}}</h6>
                    
                @endif
                <h4>
                    <a href="{{route ('user.dashboard')}}" class="btn btn-warning btn-sm float-end">Dashboard <i class="nav-icon fas fa-tachometer-alt"></i></a>
                </h4>
                <form action="{{ url('user/create-member') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="name">Member Name</label>
                            <input name="name" type="text" class="form-control" id="name" required>
                        </div>
                    </div>
                      
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="position">Position</label>
                            <input name="position" type="text" class="form-control" id="position" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="profile">Profile Image</label>
                            <input name="profile" type="file" class="form-control" id="profile" required>
                        </div>
                    </div>
                 
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
                {!! csrf_field() !!}

            </div>
        </div>
    </section>
</div>
<script>
   $('#prefill').datepicker({

});
</script>
</div>
