       <!-- Main Sidebar Container -->
       <aside class="main-sidebar sidebar-dark-primary elevation-4">
           <!-- Brand Logo -->
           <a href="index3.html" class="brand-link">
               {{-- <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: 0.8;" /> --}}
               <h4 class="text-center">School's Name</h4>
           </a>

           <!-- Sidebar -->
           <div class="sidebar">
               <!-- Sidebar user panel (optional) -->
               <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                   <div class="image">
                       <img src="{{ asset('assets/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2"
                           alt="User Image" />
                   </div>
                   <div class="info">
                       <a href="#" class="d-block">Alexander Pierce</a>
                   </div>
               </div>

               <!-- SidebarSearch Form -->
               <div class="form-inline">
                   <div class="input-group" data-widget="sidebar-search">
                       <input class="form-control form-control-sidebar" type="search" placeholder="Search"
                           aria-label="Search" />
                       <div class="input-group-append">
                           <button class="btn btn-sidebar">
                               <i class="fas fa-search fa-fw"></i>
                           </button>
                       </div>
                   </div>
               </div>

               <!-- Sidebar Menu -->
               <nav class="mt-2">
                   <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                       data-accordion="false">
                       <!-- Add icons to the links using the .nav-icon class
                   with font-awesome or any other icon font library -->

                       <li class="nav-item menu-open">
                           <a href="#" class="nav-link active">
                               <i class="nav-icon fas fa-tachometer-alt"></i>
                               <p>
                                   Dashboard
                                   <i class="right fas fa-angle-left"></i>
                               </p>
                           </a>
                           <ul class="nav nav-treeview">
                               <li class="nav-item">
                                   <a href="./index.html" class="nav-link active">
                                       <i class="far fa-circle nav-icon"></i>
                                       <p>Dashboard v1</p>
                                   </a>
                               </li>

                           </ul>
                       </li>


                   </ul>
               </nav>
               <!-- /.sidebar-menu -->
           </div>
           <!-- /.sidebar -->
       </aside>

       <!-- Content Wrapper. Contains page content -->
       <div class="content-wrapper">
           <!-- Content Header (Page header) -->
           <div class="content-header">
               <div class="container-fluid">
                   <div class="row mb-2">
                       <div class="col-sm-6">
                           <h1 class="m-0">All Member</h1>
                       </div>
                       
                   </div>
                   <!-- /.row -->
               </div>
               <!-- /.container-fluid -->
           </div>
           <!-- /.content-header -->
           <!-- Main content --> 
           <section class="content">
                <div class="container-fluid">
                    <div class="link">
                        <a href="{{ route('user.create-member') }}" class="btn btn-primary btn-sm">Add</a>
                        <a href="{{ route('user.dashboard') }}" class="btn btn-primary btn-sm">Dashboard <i class="nav-icon fas fa-tachometer-alt"></i></a>
                    </div>
                    <!-- Small boxes (Stat box) -->
                   <table class="table table-bordered">
                       <thead>
                           <tr>
                               <th class="col-md-1">ID</th>
                               <th class="col-sm-1">Profile</th>
                               <th class="col-sm-4">Name</th>
                               <th class="col-sm-4">Position</th>
                               <th class="col">Action</th>
                           </tr>
                       </thead>
                       <tbody>
                           @foreach ($member as $val)
                               <tr>
                                   <td>{{ $val->id }}</td>
                                   <td>
                                       <img src="{{ asset('uploads/member/' . $val->profile) }}"
                                           alt="profile img" width="70px" height="70px"> 
                                   </td>
                                   <td>{{ $val->name }}</td>
                                   <td>{{ $val->position }}</td>
                                   <td>
                                     {{-- <a href=""wire:click.prevent="updatemember({{ $val->id }})" class="btn btn-primary btn-sm">Edit</a> --}}
                                     {{-- <a href="{{url('user/updatemember/' .$val->id) }}" class="btn btn-primary btn-sm">Edit</a> --}}
                                     <a href="{{url('user/edit/' .$val->id) }}" class="btn btn-primary btn-sm">Edit</a>
                                     <a href="{{url('user/delete_member/'.$val->id)}}" class="btn btn-danger btn-sm">Delete</a>
                                   </td>
                               </tr>
                           @endforeach

 

                       </tbody>
                   </table>
                   <!-- /.row -->
                   <!-- Main row -->

                   <!-- /.row (main row) -->
               </div>
               <!-- /.container-fluid -->
           </section>
           <!-- /.content -->
       </div>
