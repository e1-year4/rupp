       <!-- Main Sidebar Container -->
       <aside class="main-sidebar sidebar-dark-primary elevation-4">
           <!-- Brand Logo -->
           <a href="index3.html" class="brand-link">
               {{-- <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: 0.8;" /> --}}
               <h4 class="text-center">School's Name</h4>
           </a>

           <!-- Sidebar -->
           <div class="sidebar">
               <!-- Sidebar user panel (optional) -->
               <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                   <div class="image">
                       <img src="{{ asset('assets/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2"
                           alt="User Image" />
                   </div>
                   <div class="info">
                       <a href="#" class="d-block">SOKHA</a>
                   </div>
               </div>

               <!-- SidebarSearch Form -->
               <div class="form-inline">
                   <div class="input-group" data-widget="sidebar-search">
                       <input class="form-control form-control-sidebar" type="search" placeholder="Search"
                           aria-label="Search" />
                       <div class="input-group-append">
                           <button class="btn btn-sidebar">
                               <i class="fas fa-search fa-fw"></i>
                           </button>
                       </div>
                   </div>
               </div>

               <!-- Sidebar Menu -->
               <nav class="mt-2">
                   <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                       data-accordion="false">
                       <!-- Add icons to the links using the .nav-icon class
                   with font-awesome or any other icon font library -->

                       <li class="nav-item menu-open">
                           <a href="#" class="nav-link active">
                               <i class="nav-icon fas fa-tachometer-alt"></i>
                               <p>
                                   Dashboard
                                   <i class="right fas fa-angle-left"></i>
                               </p>
                           </a>
                           <ul class="nav nav-treeview">
                               <li class="nav-item">
                                   <a href="./index.html" class="nav-link active">
                                       <i class="far fa-circle nav-icon"></i>
                                       <p>Dashboard v1</p>
                                   </a>
                               </li>
                               <li class="nav-item">
                                   <a href="./index2.html" class="nav-link">
                                       <i class="far fa-circle nav-icon"></i>
                                       <p>Dashboard v2</p>
                                   </a>
                               </li>
                               <li class="nav-item">
                                   <a href="./index3.html" class="nav-link">
                                       <i class="far fa-circle nav-icon"></i>
                                       <p>Dashboard v3</p>
                                   </a>
                               </li>
                           </ul>
                       </li>


                   </ul>
               </nav>
               <!-- /.sidebar-menu -->
           </div>
           <!-- /.sidebar -->
       </aside>

       <!-- Content Wrapper. Contains page content -->
       <div class="content-wrapper">
           <!-- Content Header (Page header) -->
           <div class="content-header">
               <div class="container-fluid">
                   <div class="row mb-2">
                       <div class="col-sm-6">
                           <h1 class="m-0">Dashboard</h1>
                       </div>
                       <!-- /.col -->
                       <div class="col-sm-6">
                           <ol class="breadcrumb float-sm-right">
                               {{-- <li class="breadcrumb-item"><a href="#">Home</a></li> --}}
                               <li class="breadcrumb-item active">Dashboard v1</li>
                           </ol>
                       </div>
                       <!-- /.col -->
                   </div>
                   <!-- /.row -->
               </div>
               <!-- /.container-fluid -->
           </div>
           <!-- /.content-header -->
           <!-- Main content -->
           <section class="content">
               <div class="container-fluid">
                   <!-- Small boxes (Stat box) -->
                   <div class="row">
                       <!-- ./col -->
                       <div class="col-lg-3 col-6">
                           <!-- small box -->
                           <div class="small-box bg-success">
                               <div class="inner">
                                   <h3>About</h3>
                                   <div class="">
                                       <a href="{{ url('user/create-member') }}">
                                           <p class="btn btn-primary">Add Member <i class="ion ion-person-add"></i></p>
                                       </a>
                                   </div>
                                   <div class="">
                                       <a href="{{ route('user.member') }}">
                                           <p class="btn btn-info">Member lists <i class="ion-navicon-round"></i></p>
                                       </a>
                                   </div>
                               </div>
                               <a href="/about">
                                   <div class="icon">
                                       <i class="ion ion-person-add"></i>
                                   </div>
                               </a>
                               <a href="/about" class="small-box-footer">View page<i
                                       class="fas fa-arrow-circle-right"></i></a>
                           </div>
                       </div>
                       {{-- menu --}}
                       <div class="col-lg-3 col-6">
                           <div class="small-box bg-info">
                               <div class="inner">
                                   <h3>Menu Page</h3>
                                   <div class="">
                                       <a href="{{ url('user/create-menu') }}">
                                           <p class="btn btn-primary">Add Foods <i class="ion ion-pizza"></i></p>
                                       </a>
                                   </div>
                                   <div class="">
                                       <a href="{{ route('user.menu') }}">
                                           <p class="btn btn-info">Foods lists <i class="ion-navicon-round"></i></p>
                                       </a>
                                   </div>
                               </div>
                               <a href="/menu">
                                   <div class="icon">
                                       <i class="ion ion-pizza"></i>
                                   </div>
                               </a>
                               <a href="/menu" class="small-box-footer">View page<i
                                       class="fas fa-arrow-circle-right"></i></a>
                           </div>
                       </div>
                       <!-- ./col -->
                       {{-- Video --}}
                       <div class="col-lg-3 col-6">
                           <div class="small-box bg-warning">
                               <div class="inner">
                                   <h3>Video Page</h3>
                                   <div class="">
                                       <a href="{{ url('user/create-video') }}">
                                           <p class="btn btn-primary">Add Video<i class="ion ion-person-add"></i></p>
                                       </a>
                                   </div>
                                   <div class="">
                                       <a href="{{ url('user/video') }}">
                                           <p class="btn btn-info">Video lists <i class="ion-android-people"></i></p>
                                       </a>
                                   </div>
                               </div>
                               <a href="/video">
                                   <div class="icon">
                                       <i class="ion-videocamera"></i>
                                   </div>
                               </a>
                               <a href="/video" class="small-box-footer">View page<i
                                       class="fas fa-arrow-circle-right"></i></a>
                           </div>
                       </div>
                       <!-- ./col -->
                       {{-- Blog --}}
                       <div class="col-lg-3 col-6">
                           <div class="small-box bg-danger">
                               <div class="inner">
                                   <h3>Blog Page</h3>
                                   <div class="">
                                       <a href="{{ url('user/create-menu') }}">
                                           <p class="btn btn-primary">Add blog<i class="ion ion-person-add"></i></p>
                                       </a>
                                   </div>
                                   <div class="">
                                       <a href="{{ route('user.menu') }}">
                                           <p class="btn btn-info">Blog lists <i class="ion-android-people"></i></p>
                                       </a>
                                   </div>
                               </div>
                               <a href="/blog">
                                   <div class="icon">
                                       <i class="ion ion-stats-bars"></i>
                                   </div>
                               </a>
                               <a href="/blog" class="small-box-footer">View page<i
                                       class="fas fa-arrow-circle-right"></i></a>
                           </div>
                       </div>
                       <!-- ./col -->
                       {{-- Contect --}}
                       <div class="col-lg-3 col-6">
                           <div class="small-box bg-danger">
                               <div class="inner">
                                   <h3>Contact Page</h3>
                                   <div class="">
                                       <a href="{{ url('user/create-menu') }}">
                                           <p class="btn btn-primary">Add Contact<i class="ion ion-person-add"></i></p>
                                       </a>
                                   </div>
                                   <div class="">
                                       <a href="{{ route('user.menu') }}">
                                           <p class="btn btn-info">lists Contact<i class="ion-android-people"></i></p>
                                       </a>
                                   </div>
                               </div>
                               <a href="/contact">
                                   <div class="icon">
                                       <i class="ion ion-stats-bars"></i>
                                   </div>
                               </a>
                               <a href="/contact" class="small-box-footer">View page<i
                                       class="fas fa-arrow-circle-right"></i></a>
                           </div>
                       </div>
                       <!-- ./col -->
                       {{-- Footer --}}
                       <div class="col-lg-3 col-6">
                           <div class="small-box bg-warning">
                               <div class="inner">
                                   <h3>Footer</h3>
                                   <div class="">
                                       <a href="{{route('user.footer') }}">
                                           <p class="btn btn-primary">Update <i class="ion ion-person-add"></i></p>
                                       </a>
                                   </div>
                                   <div class="">
                                       <a href="{{ route('pages.home') }}">
                                           <p class="btn btn-info">View on page <i class="ion-android-people"></i></p>
                                       </a>
                                   </div>
                               </div>
                               <a href="/menu" target="_blank">
                                   <div class="icon">
                                       <i class="ion ion-stats-bars"></i>
                                   </div>
                               </a>
                               <a href="/menu" target="_blank" class="small-box-footer">View page<i
                                       class="fas fa-arrow-circle-right"></i></a>
                           </div>
                       </div>
                       <!-- ./col -->


                        
                        
                   </div>
                   <!-- /.row -->
                   <!-- Main row -->

                   <!-- /.row (main row) -->
               </div>
               <!-- /.container-fluid -->
           </section>
           <!-- /.content -->
       </div>
