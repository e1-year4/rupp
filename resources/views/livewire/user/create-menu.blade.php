<div>
    {{-- Be like water. --}}
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content">
            <div class="container-fluid">
                <div class="card-body">
                    @if (session('status'))
                        <h6 class="alert alert-success">{{ session('status') }}</h6>
                    @endif
                    <h4>
                        <a href="{{ route('user.dashboard') }}" class="btn btn-warning btn-sm float-end"><i
                                class="nav-icon fas fa-tachometer-alt"></i>Dashboard </a>
                        <a href="{{ route('user.menu') }}" class="btn btn-info btn-sm float-end"><i class="ion-navicon-round"></i> Listing foods </a>
                        <a href="{{ route('pages.menu') }}" target="_blank" class="btn btn-primary btn-sm"><i
                                class="fas fa-eye"></i> View Page </a>

                    </h4>
                    <form action="{{ url('user/create-menu') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="name">Food Name</label>
                                <input name="fname" type="text" class="form-control" id="fname" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="ftype">Food type</label>
                                <select class="form-select" aria-label="Default select example" name="ftype"
                                    id="ftype">
                                    <option selected>Selects type menu</option>
                                    <option value="lunch">Lunch</option>
                                    <option value="dinner">Dinner</option>
                                    <option value="drinks">Drink</option>
                                    <option value="dessert">Dessert</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="fdetail">food detail</label>
                                <input name="fdetail" type="text" class="form-control" id="fdetail" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="fprice">price of food</label>
                                <input name="fprice" type="text" class="form-control" id="fprice" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="fimage">Food picture</label>
                                <input name="fimage" type="file" class="form-control" id="fimage" required>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </section>
    </div>
    <script>
        $('#prefill').datepicker({

        });
    </script>
</div>
