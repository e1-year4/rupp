<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="card-body">
                @if (session('status'))
                    <h6 class="alert alert-success">{{ session('status') }}</h6>
                @endif
                <h4>
                   
                    <a href="{{ route('user.dashboard') }}" class="btn btn-warning btn-sm float-end"><i
                            class="nav-icon fas fa-tachometer-alt"></i> Dashboard</a>
                    <a href="{{ route('user.video') }}" class="btn btn-info btn-sm float-end"><i
                            class="ion-navicon-round"></i> Listing video </a>
                    <a href="{{ route('pages.video') }}" target="_blank" class="btn btn-primary btn-sm"><i
                            class="fas fa-eye"></i> View Page </a>
                </h4>
                <form action="{{ url('user/create-video') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="utubelink">Link Youtube</label>
                            <input name="utubelink" type="text" class="form-control" id="utubelink" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="vname">Title</label>
                            <input name="vname" type="text" class="form-control" id="vname" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="vdetail">Detail video</label>
                            <input name="vdetail" type="text" class="form-control" id="vdetail" required>
                        </div>
                    </div>


                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </section>
</div>
<script>
    $('#prefill').datepicker({

    });
</script>
