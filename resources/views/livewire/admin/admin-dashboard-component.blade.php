   <div class="wrapper">
       <!-- Content Wrapper. Contains page content -->
       <div class="content-wrapper">
          <section class="content-header">
          <div class="card card-gray">
            <div class="toolbox">
              <a href="{{route('add.create_user')}}" class="btn btn-primary btn-sm btn-oval">
                <i class="fas fa-plus-circle"></i>Create
              </a>
            </div>
            <div class="card-block">
              <table class="table table-sm table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th></th>
                  </tr>
                  <tbody>

                    {{-- {{ $collection->links() }} --}}
                    <?php
                      $page = @$_GET['page'];
                      if(!$page)
                      {
                        $page = 1;
                      }
                      $i = config('app.row') * ($page - 1) + 1;
                    ?>


                    @php($i=1)
                    @foreach ($users as $user)
                      <tr>
                        <td>{{$i++}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                          <a href="{{url('user/delete/'.$user->id)}}" class="text-danger" title="Delete" 
                              onclick="return confirm('You want to delete?')">
                              <i class="fas fa-trash"></i>
                          </a>
                        </td>
                       
                      </tr>
                      
                    @endforeach
                  </tbody>
                </thead>
              </table>
              {{$users->links()}}
            </div>
         </div>
         </section>
      
       </div>
       <!-- /.content-wrapper -->
     
        
       <!-- /.control-sidebar -->
     </div>
     <!-- ./wrapper -->
     
     <!-- jQuery -->
     <script src="../plugins/jquery/jquery.min.js"></script>
     <!-- Bootstrap 4 -->
     <script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
     <!-- AdminLTE App -->
     <script src="../dist/js/adminlte.min.js"></script>
     <!-- AdminLTE for demo purposes -->
     <script src="../dist/js/demo.js"></script>
