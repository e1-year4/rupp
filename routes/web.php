<?php

use App\Models\Member;
use App\Models\Menus;
use App\Http\Livewire\User\File;
use App\Http\Livewire\DashboardComponent;
use Illuminate\Support\Facades\Route;
use App\Http\Livewire\User\UserDashboardComponent;
use App\Http\Livewire\User\CreateIncome;
use App\Http\Livewire\Admin\AdminDashboardComponent;
use App\Http\Controllers\CreateUserController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\VideoController;
use App\Http\Controllers\MenuController;
use App\Http\Livewire\User\CreateMember;
use App\Http\Livewire\User\MemberList;
use App\Models\Video;

use App\Http\Livewire\User\Updatemember;
use App\Http\Livewire\User\ControllerVideo;
use App\Http\Livewire\User\ControllerVideoList;
use App\Http\Livewire\User\CreateMenu;
use App\Http\Livewire\User\MenuList;
use App\Models\Menu;

use App\Http\Livewire\User\VideoList;
use App\Http\Livewire\User\CreateVideo;

use App\Http\Livewire\User\FooterList;
use App\Http\Livewire\User\CreateFooter;
use App\Models\Footer;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


// need to create middleware
// For User or Customer 
Route::middleware(['auth:sanctum','verified'])->group(function(){
    Route::get('/user/dashboard',UserDashboardComponent::class)->name('user.dashboard');
    Route::get('/user/create_income',CreateIncome::class)->name('user.create_income');
    // Route::get('/user/member',[CreateMember::class, 'view'])->name('user.member');
    Route::get('/user/member',MemberList::class)->name('user.member');
    Route::get('/user/create-member',CreateMember::class)->name('user.create-member');
    Route::post('/user/create-member',[CreateMember::class, 'store'])->name('user.create-member');
    Route::get('/user/edit/{id}','App\Http\Livewire\User\CreateMember@edit')->name('user.edit');
    Route::put('/user/edit/{update}','App\Http\Livewire\User\CreateMember@update')->name('user.update');
    Route::get('/user/delete_member/{id}', [CreateMember::class, 'delete_member']);
    
    Route::get('/user/menu',MenuList::class)->name('user.menu');
    Route::get('/user/create-menu',CreateMenu::class)->name('user.create-menu');
    Route::post('/user/create-menu',[CreateMenu::class, 'store'])->name('user.create-menu');
    Route::get('/user/edit-menu/{id}','App\Http\Livewire\User\CreateMenu@edit')->name('user.edit');
    Route::put('/user/edit-menu/{update_menu}','App\Http\Livewire\User\CreateMenu@update')->name('user.update_menu');
    Route::get('/user/delete_menu/{id}', [CreateMenu::class, 'delete_menu']);
    
    // video
    Route::get('/user/video',VideoList::class)->name('user.video');
    Route::get('/user/create-video',CreateVideo::class)->name('user.create-video');
    Route::post('/user/create-video',[CreateVideo::class, 'store'])->name('user.create-video');
    Route::get('/user/edit-video/{id}','App\Http\Livewire\User\CreateVideo@edit')->name('user.edit');
    Route::put('/user/edit-video/{update_video}','App\Http\Livewire\User\CreateVideo@update')->name('user.update_video');
    Route::get('/user/delete_video/{id}', [CreateVideo::class, 'delete_video']);

    // footer
    
    Route::get('/user/footer',FooterList::class)->name('user.footer');
    Route::get('/user/create-footer',CreateFooter::class)->name('user.create-footer');
    Route::post('/user/create-footer',[CreateFooter::class, 'store'])->name('user.create-footer');
    Route::get('/user/edit-footer/{id}','App\Http\Livewire\User\CreateFooter@edit')->name('user.edit');
    Route::put('/user/edit-footer/{update_footer}','App\Http\Livewire\User\CreateFooter@update')->name('user.update_footer');
    Route::get('/user/delete_footer/{id}', [CreateFooter::class, 'delete_footer']);


    // Route::get('user/edit/{id}','App\Http\Livewire\User\CreateVideo@edit')->name('user.editvideo');


    // Route::get('/user/edit-member', [App\Http\Controllers\CreateMember::class, 'edit'])->name('user.edit-member');
    // Route::post('/user/create-member',[App\Http\Controllers\AboutController::class, 'store'])->name('user.create-member');
     // Route::get('/user/create_member', [AboutController::class, 'c_member'])->name('create_member');

});

// For Admin
Route::middleware(['auth:sanctum','verified','authadmin'])->group(function(){
    Route::get('/admin/dashboard',AdminDashboardComponent::class)->name('admin.dashboard');
    // Route::get('/admin/create_user',AdminDashboardComponent::class)->name('create_use.Dashboard');
    Route::get('/admin/create_user',[CreateUserController::class,'create_user'])->name('add.create_user');
});

// Route::get('/user/create_user',AdminDashboardComponent::class, 'create_user')->name('create_user.create_user');

// pages
Route::get('/', [HomeController::class, 'index'])->name('pages.home');
Route::get('/about', [AboutController::class, 'about'])->name('pages.about');
Route::get('/menu', [MenuController::class, 'menu'])->name('pages.menu');
Route::get('/blog', [BlogController::class, 'blog'])->name('pages.blog');
Route::get('/contact', [ContactController::class, 'contact'])->name('pages.contact');
Route::get('/video', [VideoController::class, 'video'])->name('pages.video');

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

// require __DIR__.'/auth.php';
?>